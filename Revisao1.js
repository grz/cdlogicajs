screen.clear('blue')

//Definir largura da linha
screen.setLineWidth(2)

//Desenha forma arrendondada
screen.drawRound(0,0,100,100,'white')

//Muda a cor usada para desenhar
screen.setColor('red')

screen.drawRound(0,0,80,80)

screen.fillRound(0,0,60,60,'yellow')

//Definir largura da linha
screen.setLineWidth(4)

//Desenha retângulo
screen.drawRect(0,0,150,100,'white')

//Desenha um texto na tela
screen.drawText('Oi!!',0,0,32,'black')

//Desenha Sprite ('alien' é o nome do Sprite)
screen.drawSprite('alien',0,80,32)